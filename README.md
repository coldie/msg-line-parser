# msg-line-parser

Take home test submission for Jeffrey Thompson <coldie@coldie.net>.

# Overview

On construction, MessageProcessor creates and configures a number of Recognisers, which are added to a Tokenizer and
used to process chat messages, recognising `(emoticon)`, `@mention` and `http://urls`. A utility class determines the
title of a URL before the output is returned as a JSON string.

After construction of a MessageProcessor, `extractChatMessageJsonAsync` may be used to extract metadata from a chat
message.

![overview diagram](overview.png)

# Requirements

JDK8

# Getting Started

`gradlew clean test` will run the unit tests.

`gradlew distZip` will build a zip with the library and its runtime dependencies.

## MessageProcessor

Usage:

    MessageProcessor processor = new MessageProcessor();

    CompletableFuture<String> jsonFuture =
        processor.extractChatMessageJsonAsync("@chris you around?");

    String result = jsonFuture.get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);

# Third-Party Libraries

You may read the dependencies for yourself in build.gradle, however:

 * GSON
 * TagSoup

# Notes

 * Used TagSoup for HTML parsing to extract page titles. This is the same library used in Android for a very long time
   and its API is mostly suitable for this
 * Used GSON
 * I've used CompletableFuture to expose the API in an asynchronous manner
 * I've tried to keep the solution light and focus on the exercise rather than artificially trying to introduce more
   complex architectural patterns that wouldn't help a reader to understand the code
 * Wrote it with performance in mind, haven't benchmarked it though.
 * Explicitly avoided the use of recursion. If you want me to write something to demonstrate the use of recursion, let
   me know.
 * I did not consider configurability to be a part of the spec so some constants are hardcoded

# Assumptions

In addition to the assumptions provided in the take-home test description,
I've made the following assumptions during the development of this module.

* A null message should be treated the same as an empty message
* An empty message should still be parsed and should not throw any exceptions
* Unicode strings should be supported as far as possible
* Java's definition of whitespace is adequate
* Java's notion of a valid URL is adequate
* Only http and https URLs need to be supported
* In cases where a page can't be fetched for whatever reason, the title should be empty
* The size of the page should be limited (I specified it as 1MB)
* Because title fetching takes time, the interface to this functionality should support asynchronous operation
* Malformed HTML (HTML documents that do not parse via the chosen HTML parser) will not have their title extracted.

# Spec

    Hey Jeff,
    Please find below the take-home coding exercise.  This exercise is not meant to be tricky or complex; however, it does represent a typical problem faced by our HipChat Engineering team.
    Here are a few things to keep in mind as you work through it:
    * Please write your solution in Java.
    • Take your time and write quality, production-ready code.  Treat this as if you're a member of the HipChat Engineering team and are solving it as part of your responsibilities there. Generally candidates return this within 1 week, but if you need some additional time let me know.
    • Be thorough and take the opportunity to show the team that you've got technical chops.
    • Using frameworks and libraries is acceptable, just remember that the idea is to show off your coding abilities.
    When you think it's ready for prime time, push your work to a public repo on Bitbucket (bonus points) or Github and send us a link.
    Now, for the coding exercise...
    Please write a solution that takes a chat message string and returns a JSON string containing information about its contents. Special content to look for includes:
    1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
    2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an moniker. (https://www.hipchat.com/emoticons)
    3. Links - Any URLs contained in the message, along with the page's title.
    For example, calling your function with the following inputs should result in the corresponding return values.
    Input: "@chris you around?"
    Return (string):
    {
      "mentions": [
        "chris"
      ]
    }

    Input: "Good morning! (megusta) (coffee)"
    Return (string):
    {
      "emoticons": [
        "megusta",
        "coffee"
      ]
    }

    Input: "Olympics are starting soon; http://www.nbcolympics.com"
    Return (string):
    {
      "links": [
        {
          "link": "http://www.nbcolympics.com",
          "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
        }
      ]
    }

    Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
    Return (string):
    {
      "mentions": [
        "bob",
        "john"
      ],
      "emoticons": [
        "success"
      ],
      "links": [
        {
          "link": "https://twitter.com/jdorfman/status/430511497475670016",
          "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
        }
      ]
    }
