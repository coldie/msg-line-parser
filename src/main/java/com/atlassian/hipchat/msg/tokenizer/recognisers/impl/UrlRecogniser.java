package com.atlassian.hipchat.msg.tokenizer.recognisers.impl;

import com.atlassian.hipchat.msg.tokenizer.recognisers.Recogniser;
import com.atlassian.hipchat.msg.util.TitleFetcher;
import com.atlassian.hipchat.msg.tokenizer.Token;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class UrlRecogniser extends Recogniser {
    //we're going to track quotes as we progress so that we can unwrap them where appropriate
    private final Deque<QuotePair> quotePairStack = new ArrayDeque<>();

    private static final List<QuotePair> quotePairs = Arrays.asList(
        new QuotePair('"'),
        new QuotePair('(', ')'),
        new QuotePair('[', ']'),
        new QuotePair('\'')
    );

    private static final List<String> supportedSchemes = Arrays.asList(
            "http",
            "https"
    );

    @Override
    public Token recognise(String input, int position) {
        updateQuoteStack(input.charAt(position));

        int scanPosition;
        int endOfToken = position;


        // detect the start of a URL based on our supported schemes
        if (!supportedSchemes.stream().anyMatch(s -> input.substring(position).startsWith(s))) {
            return null;
        }

        StringBuilder candidateString = new StringBuilder();

        Character nextCharacter;
        Character current;

        for (scanPosition = position; scanPosition < input.length(); scanPosition++) {
            current = input.charAt(scanPosition);
            nextCharacter = (scanPosition >= input.length()-1) ? null : input.charAt(scanPosition + 1);

            if (!shouldContinueScanning(current, nextCharacter)) {
                break;
            }

            candidateString.append(current);

            endOfToken = scanPosition;
        }

        Token<Link> urlToken;

        URL parsedUrl;
        try {
            // rely on Java's URL parser to do the validation
            parsedUrl = new URL(candidateString.toString());
        } catch (MalformedURLException e) {
            //URL was not valid!
            return null;
        }

        String title = TitleFetcher.fetchTitle(parsedUrl);

        Link link = new Link(parsedUrl.toString(), title);

        urlToken = new Token<>(input.substring(position, endOfToken + 1), link);
        return urlToken;

    }

    private boolean shouldContinueScanning(Character current, Character nextCharacter) {
        // there is no character here. stop scanning.
        if (current == null) {
            return false;
        }

        // this is not a valid URL character. stop scanning
        if (!validUrlCharacter(current)) {
            return false;
        }

        // if this is not an endquote we don't need to consider it in any more detail at this point
        if (!isEndQuoteCharacter(current)) {
            return true;
        }

        if (isNextCharacterTerminal(nextCharacter)) {
            return false;
        }

        // otherwise this must be a valid URL character
        return true;
    }

    private boolean isNextCharacterTerminal(Character nextCharacter) {
        // is there at least one more character in front? if not, we have to stop here
        if (nextCharacter == null) {
            return true;
        }

        //is the character possibly still part of this URL?
        return !validUrlCharacter(nextCharacter) && !isEndQuoteCharacter(nextCharacter);
    }

    private boolean isEndQuoteCharacter(Character cur) {
        return !quotePairStack.isEmpty() && quotePairStack.peek().isEnd(cur);
    }

    private static boolean validUrlCharacter(Character cur) {
        final String alphanumericChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$-_.+!*'()";
        final String reservedChars = "$&+,/:;=?@";
        final String escapeChar = "%";

        return alphanumericChars.indexOf(cur) >= 0
                || reservedChars.indexOf(cur) >= 0
                || escapeChar.indexOf(cur) >= 0;
    }

    private void updateQuoteStack(char c) {
        //if c matches end char of quote pair on top of stack, pop, else if c is in the quote characters list, push
        if (isEndQuoteCharacter(c)) {
            quotePairStack.pop();
            return;
        }

        for (QuotePair quotePair : quotePairs) {
            if (quotePair.isStart(c)) {
                quotePairStack.push(quotePair);
                break;
            }
        }
    }

    private static class QuotePair {
        private final Character start;
        private final Character end;

        public QuotePair(Character start, Character end) {
            this.start = start;
            this.end = end;
        }

        public QuotePair(Character startEnd) {
            start = end = startEnd;
        }

        public boolean isStart(Character s) {
            return s.compareTo(start) == 0;
        }

        public boolean isEnd(Character s) {
            return s.compareTo(end) == 0;
        }
    }

    /**
     * Represents a Link in a chat message with a URL and a Title
     */
    public static class Link {
        private final String url;
        private final String title;

        public Link(String url, String title) {
            this.url = url;
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public String getTitle() {
            return title;
        }
    }
}
