package com.atlassian.hipchat.msg.tokenizer.recognisers.impl;

import com.atlassian.hipchat.msg.tokenizer.recognisers.Recogniser;
import com.atlassian.hipchat.msg.tokenizer.Token;

public class MentionRecogniser extends Recogniser {
    @Override
    public Token recognise(String input, int position) {
        if (input.charAt(position) == '@') {
            StringBuilder userName = new StringBuilder();

            // scan the input for the next non-word character
            int last = position;
            for (int i = position+1; i < input.length(); i++) {
                Character cur = input.charAt(i);
                last = i;

                if (Character.isLetterOrDigit(cur)) {
                    userName.append(cur);
                } else {
                    break;
                }
            }

            if (userName.length() > 0) {
                return new Token<>(input.substring(position, last), new Mention(userName.toString()));
            }
        }
        return null;
    }

    /**
     * Represents a Mention, someone's user name that shows up in chat after an `@' symbol
     */
    public static class Mention {
        private final String userName;

        public Mention(String userName) {
            this.userName = userName;
        }

        public String getUserName() {
            return userName;
        }
    }
}
