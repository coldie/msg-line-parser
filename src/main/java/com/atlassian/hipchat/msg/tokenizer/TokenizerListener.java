package com.atlassian.hipchat.msg.tokenizer;

public interface TokenizerListener {
    void notify(Token recognisedToken);
}
