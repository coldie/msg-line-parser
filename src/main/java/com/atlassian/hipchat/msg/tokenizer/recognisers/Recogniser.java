package com.atlassian.hipchat.msg.tokenizer.recognisers;

import com.atlassian.hipchat.msg.tokenizer.Token;

/**
 * A recogniser examines the input string starting at a particular position and attempts to recognise its token.
 *
 * It is used by the Tokenizer to recognise meaningful tokens from the input string.
 */
public abstract class Recogniser {
    /**
     * recognise is called 0 or more times on an input string to recognise meaningful tokens from an input string.
     * @param input The input string to attempt to recognise a token within
     * @param position The position in the input string to attempt to recognise a token
     * @return Any recognised tokens in the input string
     */
    public abstract Token recognise(String input, int position);
}
