package com.atlassian.hipchat.msg.tokenizer;

public class Token<T> {
    public final String originalCharacters;
    public final T recognisedToken;

    public Token(String characters, T recognisedToken) {
        this.originalCharacters = characters;
        this.recognisedToken = recognisedToken;
    }
}
