package com.atlassian.hipchat.msg.tokenizer;

import com.atlassian.hipchat.msg.tokenizer.recognisers.Recogniser;

import java.util.LinkedList;

public class Tokenizer {
    // the list of recognisers we're going to use to try and find the tokens
    private final LinkedList<Recogniser> recognisers = new LinkedList<>();
    private TokenizerListener listener;

    public void addRecogniser(Recogniser recogniser) {
        this.recognisers.add(recogniser);
    }

    public void tokenize(final String input) {
        if (input == null) {
            return;
        }

        int currentPosition = 0; // first character in the input
        int inputLength = input.length();

        // scan the input string character by character
        while (currentPosition < inputLength) {
            Token recognised;

            // call each recogniser. Yo dawg I heard you like recognisers...
            for (Recogniser recogniser : recognisers) {
                recognised = recogniser.recognise(input, currentPosition);
                //did we find a token?
                if (recognised != null) {
                    //yes! let's move our position to the character after the recognised token ends
                    currentPosition = currentPosition + recognised.originalCharacters.length();

                    //notify our listener we have found a token
                    notifyListener(recognised);

                    //we're only allowing one token to be recognised at a time so break here
                    break;
                }
            }

            // no token was recognised at this position; we're just going to advance as we aren't interested
            // in the actual chat message for the purpose of this exercise
            currentPosition++;
        }
    }

    private void notifyListener(Token recognised) {
        if (listener != null)
            listener.notify(recognised);
    }

    public void setListener(TokenizerListener listener) {
        this.listener = listener;
    }
}