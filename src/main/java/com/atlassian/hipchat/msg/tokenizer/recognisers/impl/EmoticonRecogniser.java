package com.atlassian.hipchat.msg.tokenizer.recognisers.impl;

import com.atlassian.hipchat.msg.tokenizer.recognisers.Recogniser;
import com.atlassian.hipchat.msg.tokenizer.Token;

public class EmoticonRecogniser extends Recogniser {

    private static final int MAX_EMOTICON_LENGTH = 16;
    private static final char EMOTICON_START_CHARACTER = '(';
    private static final char EMOTICON_END_CHARACTER = ')';

    @Override
    public Token recognise(String input, int tokenPosition) {
        if (input.charAt(tokenPosition) == EMOTICON_START_CHARACTER) {
            int lastIndexToConsider = Math.min(input.length()-1, tokenPosition + MAX_EMOTICON_LENGTH);

            //used to accumulate the recognised emoticon characters
            StringBuilder emoticon = new StringBuilder();

            boolean closed = false;
            int scanPosition;
            for (scanPosition = tokenPosition + 1; scanPosition <= lastIndexToConsider && !closed; scanPosition++) {
                Character cur = input.charAt(scanPosition);

                if (cur == EMOTICON_END_CHARACTER) {
                    closed = true;
                } else if (Character.isLetterOrDigit(input.codePointAt(scanPosition))) {
                    emoticon.append(cur);
                }
                else {
                    break;
                }
            }

            int endOfToken = scanPosition;

            if (closed && emoticon.length() > 0) {
                return new Token<>(input.substring(tokenPosition, endOfToken),
                        new Emoticon(emoticon.toString()));
            }
        }
        return null;
    }

    /**
     * Represents an Emoticon in a chat message with a Moniker to identify it
     */
    public static class Emoticon {
        private final String moniker;

        public Emoticon(String moniker) {
            this.moniker = moniker;
        }

        public String getMoniker() {
            return moniker;
        }
    }
}
