package com.atlassian.hipchat.msg;

import com.atlassian.hipchat.msg.tokenizer.TokenizerListener;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.EmoticonRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.MentionRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.UrlRecogniser;
import com.atlassian.hipchat.msg.tokenizer.Tokenizer;
import com.atlassian.hipchat.msg.util.JsonOutputTokenizerListener;

import java.util.concurrent.CompletableFuture;

/**
 * A MessageProcessor exposes an asynchronous interface allowing a JSON string representation of any emoticons, mentions
 * and links to be extracted from a string.
 *
 * @author Jeffrey Thompson
 */
class MessageProcessor {
    /**
     * @return a Tokenizer configured to recognize Emoticons, Mentions and Links
     */
    private static Tokenizer createTokenizer() {
        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new EmoticonRecogniser());
        tokenizer.addRecogniser(new MentionRecogniser());
        tokenizer.addRecogniser(new UrlRecogniser());
        return tokenizer;
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return A JSON representation of the mentions, emoticons and links in the @param(chatMessage)
     */
    private static String extractChatMessageJson(String chatMessage) {
        Tokenizer tokenizer = createTokenizer();
        TokenizerListener tokenizerListener = new JsonOutputTokenizerListener();
        tokenizer.setListener(tokenizerListener);
        tokenizer.tokenize(chatMessage);

        return tokenizerListener.toString();
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return A CompletableFuture that, when complete, will provide a JSON representation of the mentions,
     * emoticons and links in the @param(chatMessage)
     */
    public CompletableFuture<String> extractChatMessageJsonAsync(String chatMessage) {
        return CompletableFuture.supplyAsync(() -> extractChatMessageJson(chatMessage));
    }
}
