package com.atlassian.hipchat.msg.util;

import com.atlassian.hipchat.msg.tokenizer.Token;
import com.atlassian.hipchat.msg.tokenizer.TokenizerListener;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.EmoticonRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.MentionRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.UrlRecogniser;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedList;

public class JsonOutputTokenizerListener implements TokenizerListener {
    private final LinkedList<MentionRecogniser.Mention> mentions = new LinkedList<>();
    private final LinkedList<EmoticonRecogniser.Emoticon> emoticons = new LinkedList<>();
    private final LinkedList<UrlRecogniser.Link> links = new LinkedList<>();

    @Override
    public void notify(Token token) {
        Object recognisedToken = token.recognisedToken;

        if (MentionRecogniser.Mention.class.isInstance(recognisedToken)) {
            mentions.add((MentionRecogniser.Mention) recognisedToken);
        } else if (EmoticonRecogniser.Emoticon.class.isInstance(recognisedToken)) {
            emoticons.add((EmoticonRecogniser.Emoticon) recognisedToken);
        } else if (UrlRecogniser.Link.class.isInstance(recognisedToken)) {
            links.add((UrlRecogniser.Link) recognisedToken);
        }
    }

    @Override
    public String toString() {
        try (StringWriter output = new StringWriter()) {
            Gson gson = new Gson();
            // love the new try using resources syntax
            try (JsonWriter writer = gson.newJsonWriter(output)) {
                writer.beginObject();
                if (!mentions.isEmpty()) {
                    writeMentions(writer);
                }
                if (!emoticons.isEmpty()) {
                    writeEmoticons(writer);
                }

                if (!links.isEmpty()) {
                    writeLinks(writer);
                }
                writer.endObject();
            }
            return output.toString();
        } catch (IOException e) {
            //error serializing. return null.
            Logger.warning("IO Exception trying to serialize chat message. Returning empty string.", e);
            return "";
        }
    }

    private void writeLinks(JsonWriter writer) throws IOException {
        writer.name("links");
        writer.beginArray();
        for (UrlRecogniser.Link u : links) {
            writer.beginObject();
            writer.name("link");
            writer.value(u.getUrl());
            writer.name("title");
            writer.value(u.getTitle());
            writer.endObject();
        }
        writer.endArray();
    }

    private void writeEmoticons(JsonWriter writer) throws IOException {
        writer.name("emoticons");
        writer.beginArray();
        for (EmoticonRecogniser.Emoticon m : emoticons) {
            writer.value(m.getMoniker());          //       "success"
        }
        writer.endArray();                          //    ],
    }

    private void writeMentions(JsonWriter writer) throws IOException {
        writer.name("mentions");                    //
        writer.beginArray();                        //
        for (MentionRecogniser.Mention m : mentions) {                //
            writer.value(m.getUserName());          //       "john"
        }                                           //
        writer.endArray();                          //    ],
    }
}
