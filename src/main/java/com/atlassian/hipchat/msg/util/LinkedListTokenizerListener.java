package com.atlassian.hipchat.msg.util;

import com.atlassian.hipchat.msg.tokenizer.Token;
import com.atlassian.hipchat.msg.tokenizer.TokenizerListener;

import java.util.LinkedList;

/**
 * A linked list that Listens to the tokenizer for recognised tokens and adds them to itself.
 */
public class LinkedListTokenizerListener extends LinkedList<Token> implements TokenizerListener {
    @Override
    public void notify(Token recognisedToken) {
        add(recognisedToken);
    }
}
