package com.atlassian.hipchat.msg.util;

import java.util.logging.Level;

/**
 * Logger utility
 */
class Logger {
    private static final String HIPCHAT_LOGGER_NAME = "hipchat.msg";
    private static java.util.logging.Logger theLogger = null;

    static {
        theLogger = getLogger();
    }

    private Logger() {
        // private constructor to hide public default
    }

    private static java.util.logging.Logger getLogger() {
        if (theLogger == null) {
            theLogger = java.util.logging.Logger.getLogger(HIPCHAT_LOGGER_NAME);
        }
        return theLogger;
    }

    public static void warning(String message, Throwable t) {
        getLogger().log(Level.WARNING, message, t);
    }

    public static void swallow(@SuppressWarnings("SameParameterValue") String message, Throwable throwable) {
        getLogger().log(Level.FINEST, message, throwable.getClass().getSimpleName());
    }
}