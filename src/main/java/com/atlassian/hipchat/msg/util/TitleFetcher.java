package com.atlassian.hipchat.msg.util;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utility method to fetch a title for a URL
 */
public class TitleFetcher {

    private static final int MAX_DOCUMENT_LENGTH_BYTES = 1048576;

    private TitleFetcher() {
        // explicit private constructor to hide public default
    }

    public static String fetchTitle(URL url) {
        StringBuilder titleBuilder = new StringBuilder();

        try {
            URLConnection urlConnection = url.openConnection();
            long maxDocumentSizeBytes = MAX_DOCUMENT_LENGTH_BYTES;
            if (urlConnection.getContentLengthLong() > maxDocumentSizeBytes) {
                throw new IOException("URL " + url + " is over 1MB, not scanning it for a title");
            }
            try (InputStream inputStream = urlConnection.getInputStream()) {
                SAXParserImpl.newInstance(null).parse(
                        inputStream,
                        new TitleExtractingXmlHandler(titleBuilder)
                );
            }
        }
        catch (NonFatalFoundTitleException ignored) {
            // work around API in SAX not allowing handlers to end processing
            Logger.swallow("Found title, no need to process the rest of the file", ignored);
        } catch (SAXException t) {
            Logger.warning("Unable to process URL: ", t);
        } catch (IOException t) {
            //any errors here we're just going to return an empty string for the title
            Logger.warning("Unable to open connection to URL: ", t);
        }

        return titleBuilder.toString();
    }

    private static class TitleExtractingXmlHandler extends DefaultHandler {
        private final StringBuilder titleBuilder;
        private boolean insideTitle;

        public TitleExtractingXmlHandler(StringBuilder titleBuilder) {
            this.titleBuilder = titleBuilder;
        }

        @Override
        public void startElement(String uri, String localName,
                                 String qName, Attributes a)
        {
            if ("title".equalsIgnoreCase(qName)) {
                //inside title
                insideTitle = true;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (insideTitle) {
                titleBuilder.append(new String(ch, start, length));
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if ("title".equalsIgnoreCase(qName)) {
                //inside title
                insideTitle = false;
                throw new NonFatalFoundTitleException();
            }
        }
    }

    private static class NonFatalFoundTitleException extends SAXException {
    }
}
