package com.atlassian.hipchat.msg;

import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.EmoticonRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.MentionRecogniser;
import com.atlassian.hipchat.msg.tokenizer.recognisers.impl.UrlRecogniser;
import com.atlassian.hipchat.msg.util.LinkedListTokenizerListener;
import com.atlassian.hipchat.msg.tokenizer.Tokenizer;
import org.junit.Test;

import static org.junit.Assert.*;

public class TokenizerTest {
    @Test
    public void testMentionsTokenizer() throws Exception {
        //arrange
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new MentionRecogniser());
        tokenizer.setListener(tokenList);

        //act
        tokenizer.tokenize("hello @jeff @@@@ @@jeff@ coldie@coldie.net");

        //assert
        assertEquals("Expect to see 3 mentions", 3, tokenList.size());
        //TODO: assert the types of tokens recognised
    }

    @Test
    public void testEmoticons() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new EmoticonRecogniser());
        tokenizer.setListener(tokenList);

        tokenizer.tokenize("(megusta) (coffee) () ((( )) ()())) (thisisalongone1) (aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa) (but this is not actually a custom one)");

        assertEquals("Expect to see 3 emoticons", 3, tokenList.size());
        assertEquals("Expected megusta", "megusta", ((EmoticonRecogniser.Emoticon)tokenList.get(0).recognisedToken).getMoniker());
        assertEquals("Expected coffee", "coffee", ((EmoticonRecogniser.Emoticon)tokenList.get(1).recognisedToken).getMoniker());
        assertEquals("Expected thisisalongone1", "thisisalongone1", ((EmoticonRecogniser.Emoticon)tokenList.get(2).recognisedToken).getMoniker());
    }

    @Test
    public void testUrl() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new UrlRecogniser());
        tokenizer.setListener(tokenList);
        tokenizer.tokenize("@jeff jeff@foo.com (megusta) https://www.google.com/ http://www.nbc.com/ (coffee) () ((( )) ()())) (thisisalongone1) (aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa) (but this is not actually a custom one)");

        assertEquals("Expect to see 2 URLs", 2, tokenList.size());
        assertEquals("Expect Google to be first link's title", "Google", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getTitle());
        assertEquals("Expect https://www.google.com/ to be first link's url", "https://www.google.com/", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
    }

    @Test
    public void testTrickyUrl() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new UrlRecogniser());
        tokenizer.setListener(tokenList);
        tokenizer.tokenize("this is an annoying (http://www.quoted.url.com/) that should ignore the last parenthesis");

        assertEquals("Expect to see 1 URL", 1, tokenList.size());
        assertEquals("Expect URL not to contain parentheses", "http://www.quoted.url.com/", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
    }


    @Test
    public void testTrickyUrlWithEndQuote() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new UrlRecogniser());
        tokenizer.setListener(tokenList);
        tokenizer.tokenize("this is an annoying (http://www.quoted.url.com/(blaat)) that should ignore the last parenthesis only");

        assertEquals("Expect to see 1 URL", 1, tokenList.size());
        assertEquals("Expect URL to contain parentheses", "http://www.quoted.url.com/(blaat)", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
    }


    @Test
    public void testBadUrlWithNoTitleReturnsEmptyString() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new UrlRecogniser());
        tokenizer.setListener(tokenList);
        tokenizer.tokenize("https://bitbucket.org/api/2.0/users/coldie/");

        assertEquals("Expect to see 1 URL", 1, tokenList.size());
        assertEquals("Expect URL be https://bitbucket.org/api/2.0/users/coldie/", "https://bitbucket.org/api/2.0/users/coldie/", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
        assertEquals("Expect Title to be empty", "https://bitbucket.org/api/2.0/users/coldie/", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
    }

    @Test
    public void testUrlWithParenthesisInside() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new UrlRecogniser());
        tokenizer.setListener(tokenList);
        String input = "this is an annoying (https://msdn.microsoft.com/en-us/library/aa752574(VS.85).aspx)";
        tokenizer.tokenize(input);

        assertEquals("Expect to see 1 URL", 1, tokenList.size());
        assertEquals("Expect URL to contain parentheses", "https://msdn.microsoft.com/en-us/library/aa752574(VS.85).aspx", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getUrl());
        assertEquals("Expect Title to match to contain parentheses", "IHTMLDocument2 interface (Windows)", ((UrlRecogniser.Link)tokenList.getFirst().recognisedToken).getTitle());
    }

    @Test
    public void testRTLMessageWithMentionAndEmoticon() throws Exception {
        LinkedListTokenizerListener tokenList = new LinkedListTokenizerListener();

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new MentionRecogniser());
        tokenizer.addRecogniser(new EmoticonRecogniser());
        tokenizer.setListener(tokenList);
        String input = "@alexe ، بائیں مزہ ہے دائیں ، یہ نہیں ہے؟ (evilsmile)";
        tokenizer.tokenize(input);

        assertEquals("Expect to see 2 tokens", 2, tokenList.size());
        assertEquals("Expect first token to be the mention @alexe", "alexe", ((MentionRecogniser.Mention)tokenList.get(0).recognisedToken).getUserName());
        assertEquals("Expect second token to be the emoticon (evilsmile)", "evilsmile", ((EmoticonRecogniser.Emoticon)tokenList.get(1).recognisedToken).getMoniker());
    }
}