package com.atlassian.hipchat.msg;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class MessageProcessorTest {

    private final static long TIMEOUT_MILLISECONDS = 10000; // 10 second timeout

    @Test
    public void testEmptyMessage() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture = processor.extractChatMessageJsonAsync("");

        String result = jsonFuture.get();
        assertEquals("Empty JSON response when empty string passed in", "{}", result);
    }

    @Test
    public void testNullMessage() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture = processor.extractChatMessageJsonAsync(null);

        String result = jsonFuture.get();
        assertEquals("Empty JSON response when null string passed", "{}", result);

    }

    @Test
    public void testProvidedExample1() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture =
                processor.extractChatMessageJsonAsync("@chris you around?");

        String result = jsonFuture.get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);
        assertEquals("Provided example 1 works", "{\"mentions\":[\"chris\"]}", result);
    }

    @Test
    public void testProvidedExample2() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture =
                processor.extractChatMessageJsonAsync("Good morning! (megusta) (coffee)");

        String result = jsonFuture.get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);
        assertEquals("Provided example 2 works", "{\"emoticons\":[\"megusta\",\"coffee\"]}", result);
    }

    @Test
    public void testProvidedExample3() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture =
                processor.extractChatMessageJsonAsync("Olympics are starting soon; http://www.nbcolympics.com");

        String result = jsonFuture.get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);
        assertEquals("Provided example 3 works", "{\"links\":[{\"link\":\"http://www.nbcolympics.com\",\"title\":\"NBC Olympics | Home of the 2016 Olympic Games in Rio\"}]}", result);
    }

    @Test
    public void testProvidedExample4() throws Exception {
        MessageProcessor processor = new MessageProcessor();

        CompletableFuture<String> jsonFuture =
                processor.extractChatMessageJsonAsync("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");

        String result = jsonFuture.get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);
        assertEquals("Provided example 4 works", "{\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"],\"links\":[{\"link\":\"https://twitter.com/jdorfman/status/430511497475670016\",\"title\":\"Justin Dorfman on Twitter: \\\"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\\\"\"}]}", result);
    }
}